package main

import (
	"encoding/json"
	"net/http"
	"reflect"
)

const testDemo = "https://www.w3schools.com/xml/tempconvert.asmx?WSDL"

type Celcius struct {
	Fahrenheit string `json:"fahrenheit"`
}

type Result struct {
	Celsius float64 `xml:"FahrenheitToCelsiusResult"`
}

func getF(w http.ResponseWriter, r *http.Request) {
	creds := Celcius{}
	v := &Result{}

	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	data, err := SOAP(ConvertFToMap(creds), testDemo, "FahrenheitToCelsius", v)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	JSONW(w, data)
}

func ConvertFToMap(t Celcius) (params map[string]interface{}) {
	params = make(Params, 0)
	s := reflect.ValueOf(&t).Elem()

	typeOfT := s.Type()

	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		params[typeOfT.Field(i).Name] = f.Interface()
	}
	return
}
