package main

import (
	"encoding/xml"
	"fmt"

	soap "github.com/viknesh-nm/soap-go"
)

const wsdl = "https://dguat.securitybank.com/DigiIBFTv4/service.asmx?op="

func SOAP(params map[string]interface{}, urlPath string, xmlDataField string, v interface{}) (interface{}, error) {
	res, err := soap.NewClient(urlPath)
	if err != nil {
		fmt.Printf("error not expected: %s", err)
		return nil, err
	}
	se, err := res.GetData(xmlDataField, params)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	err = xml.Unmarshal([]byte(se), v)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return v, nil
}
