package main

import (
	"encoding/json"
	"net/http"
	"reflect"
)

type Params map[string]interface{}

type BankListRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type PaymentRequest struct {
	Username              string `json:"username"`
	Bank                  string `json:"bank"`
	Password              string `json:"password"`
	AcctTo                string `json:"acctTo"`
	Amount                string `json:"amount"`
	SenderFirstName       string `json:"senderFirstName"`
	SenderMidName         string `json:"senderMidName"`
	SenderLastName        string `json:"senderLastName"`
	SenderAddressLine1    string `json:"senderAddressLine1"`
	SenderAddressLine2    string `json:"senderAddressLine2"`
	SenderCity            string `json:"senderCity"`
	SenderStateProv       string `json:"senderStateProv"`
	SenderPostalCode      string `json:"senderPostalCode"`
	SenderBirthdate       string `json:"senderBirthdate"`
	SenderBirthPlace      string `json:"senderBirthPlace"`
	SenderNatureOfWork    string `json:"senderNatureOfWork"`
	SenderContactDetails  string `json:"senderContactDetails"`
	SenderSourceOfFunds   string `json:"senderSourceOfFunds"`
	SenderNationality     string `json:"senderNationality"`
	PrimaryIDType         string `json:"primaryIDType"`
	PrimaryIDNo           string `json:"primaryIDNo"`
	SecondaryIDType1      string `json:"secondaryIDType1"`
	SecondaryIDNo1        string `json:"secondaryIDNo1"`
	SecondaryIDType2      string `json:"secondaryIDType2"`
	SecondaryIDNo2        string `json:"secondaryIDNo2"`
	OriginatingCountry    string `json:"originatingCountry"`
	RecipientMidName      string `json:"recipientMidName"`
	RecipientFirstName    string `json:"recipientFirstName"`
	RecipientLastName     string `json:"recipientLastName"`
	RecipientAddressLine1 string `json:"recipientAddressLine1"`
	RecipientAddressLine2 string `json:"recipientAddressLine2"`
	RecipientCity         string `json:"recipientCity"`
	RecipientStateProv    string `json:"recipientStateProv"`
	TraceNo               string `json:"traceNo"`
}

func BankListHandler(w http.ResponseWriter, r *http.Request) {
	var (
		creds    = BankListRequest{}
		response interface{}
	)
	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	paramsData := ConvertBankFieldToMap(creds)

	data, err := SOAP(paramsData, wsdl+"getListOfBanks", "getListOfBanks", response)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	JSONW(w, data)
}

func ConvertBankFieldToMap(t BankListRequest) (params map[string]interface{}) {
	params = make(Params, 0)
	s := reflect.ValueOf(&t).Elem()

	typeOfT := s.Type()

	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		params[typeOfT.Field(i).Name] = f.Interface()
	}
	return
}

func PayBankHandler(w http.ResponseWriter, r *http.Request) {

	var (
		req      = PaymentRequest{}
		response interface{}
	)

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	paramsData := ConvertPayBankFieldToMap(req)

	data, err := SOAP(paramsData, wsdl+"payBank", "payBank", response)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	JSONW(w, data)
}

func ConvertPayBankFieldToMap(t PaymentRequest) (params map[string]interface{}) {
	params = make(Params, 0)
	s := reflect.ValueOf(&t).Elem()

	typeOfT := s.Type()

	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		params[ToCamelCase(typeOfT.Field(i).Name)] = f.Interface()
	}
	return
}
