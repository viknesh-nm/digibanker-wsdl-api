package main

import (
	"flag"
	"fmt"
	"net/http"
)

var port = flag.String("port", "", "Port Address")

func main() {
	flag.Parse()
	route := InitRoutes()

	if *port == "" {
		*port = "8080"
	}

	fmt.Printf("Listening to the port:%s\n", *port)
	http.ListenAndServe(":"+*port, route)

}
