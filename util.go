package main

import (
	"encoding/json"
	"net/http"
	"unicode"
)

// JSONW writes JSON response to the given writer
func JSONW(w http.ResponseWriter, data interface{}) {
	d, err := json.MarshalIndent(data, "", "  ")
	w.Header().Set("Content-Type", "application/json")

	_, err = w.Write(d)
	if nil != err {
		return
	}
	_, err = w.Write([]byte("\n"))
	if nil != err {
		return
	}
}

func ToCamelCase(input string) string {
	a := []rune(input)
	a[0] = unicode.ToLower(a[0])
	return string(a)
}
