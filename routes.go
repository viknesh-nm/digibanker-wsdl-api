package main

import "github.com/gorilla/mux"

func InitRoutes() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/list", BankListHandler).Methods("POST")
	r.HandleFunc("/pay_bank", PayBankHandler).Methods("POST")

	// demo test
	r.HandleFunc("/demo", getF).Methods("POST")
	return r
}
